package AttributeGrouping;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AttributeGrouping {
    public static void main(String[] args) {
		try {
			List<ClusterClass> cluster_list = new ArrayList();
			
			Dbase db = new Dbase();
			Connection con = db.dbase();
			//Insert Results into 2-D lists
			
			String query1 = "SELECT raw_category_attributes.attribute_name,"
					+ " raw_product.id, raw_product.product_title,"
					+ " raw_products_attributes_value.attribute_absolute_id,"
					+ " raw_products_attributes_value.attribute_value"
				+ " FROM raw_product "
				+ " INNER JOIN raw_products_attributes_value"
				+ " ON raw_product.id = raw_products_attributes_value.product_id"
				+ " INNER JOIN raw_category_attributes"
				+ " ON raw_category_attributes.attributes_absolute_id = raw_products_attributes_value.attribute_absolute_id"
				+ " WHERE id = ";
            String query_number = "SELECT id "
            	+ "FROM raw_product ORDER BY id DESC";
            ResultSet rs_number = con.prepareStatement(query_number).executeQuery();
            rs_number.next();
            int number_of_rows = rs_number.getInt("id");
            rs_number.close();
                        
            for(int i=1; i<=number_of_rows+1; i++){
//            for(int i=1; i<=100; i++){
            	ResultSet rs_1 = con.prepareStatement(query1+i).executeQuery();
                if(rs_1.next()){    
	            	ProductObject retrieved_product = new ProductObject();
	            	retrieved_product.setID(rs_1.getInt("id"));
	            	retrieved_product.setTitle(rs_1.getString("product_title"));

	            	List<ProductAttributeClass> attribute_list = new ArrayList();
	                while(rs_1.next()){
	                	ProductAttributeClass attribute_object = new ProductAttributeClass();
	                	attribute_object.setID(rs_1.getInt("attribute_absolute_id"));
	                	attribute_object.setName(rs_1.getString("attribute_name"));
	                	attribute_object.setValue(rs_1.getString("attribute_value"));
	                    attribute_list.add(attribute_object);
	                }
	                retrieved_product.setAttributes(attribute_list);
	                
				    if(retrieved_product.getID() > 0){
				    	List<ProductObject> temp_product_list = new ArrayList();
				    	temp_product_list.add(retrieved_product);
				    	ClusterClass temp_cluster = new ClusterClass();
				    	temp_cluster.setProductList(temp_product_list);
				    	cluster_list.add(temp_cluster);
				    }                                                        
	                rs_1.close();
	                if(i%200 == 0){
	                	System.out.println(i);
	                }
                }
            }
            System.out.println("First Loop");
            
            AttributeGrouping grouping_object = new AttributeGrouping();
            List<ClusterClass> out_cluster_list = new ArrayList();
            while(cluster_list.size()>0){
            	boolean flag_did_match = false;
            	for(int i_iterator=0; i_iterator<out_cluster_list.size(); i_iterator++){
		        	boolean check1 = grouping_object.compare_product_names(cluster_list.get(0), out_cluster_list.get(i_iterator));
		        	boolean check2 = grouping_object.compare_attributes(cluster_list.get(0), out_cluster_list.get(i_iterator));
		        	if(check1 && check2){
            			out_cluster_list = grouping_object.clusterMatched(out_cluster_list, cluster_list.remove(0), out_cluster_list.remove(i_iterator));
            			flag_did_match = true;
            			break;
            		}
            	}
            	if(!flag_did_match){
            		out_cluster_list.add(cluster_list.remove(0));
            	}
            }
            System.out.println("Second Loop");
            
			grouping_object.store_clusters(out_cluster_list, con);
			                                         
            con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void store_clusters(List<ClusterClass> cluster_list, Connection con) {
		try{
			for(int i_main=0; i_main<cluster_list.size(); i_main++){
				ClusterClass cluster_to_store = cluster_list.get(i_main);
				List<ProductObject> product_list_to_store = cluster_to_store.getProductList();
				for(int j_main=0; j_main<product_list_to_store.size(); j_main++){
				    ProductObject product_to_store = product_list_to_store.get(j_main);
					int product_id = product_to_store.getID();
				    String product_title = product_to_store.getTitle();
					
				    String query = "INSERT INTO clustering (cluster_id, product_id, product_name, host) VALUES (?, ?, ?, ?)";
				    PreparedStatement pre_smt = con.prepareStatement(query);
				    pre_smt.setInt(1, i_main+1);
				    pre_smt.setInt(2, product_id);
				    pre_smt.setString(3, product_title);
				    pre_smt.setString(4, "host");
				    pre_smt.execute();
				    pre_smt.close();
			    }
		    }
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	private List<ClusterClass> clusterMatched(List<ClusterClass> out_cluster_list, ClusterClass cluster_1, ClusterClass cluster_2) {
		ClusterClass cluster_to_store = new ClusterClass();
		cluster_to_store.setProductList(cluster_1.getProductList());
		cluster_to_store.addProductList(cluster_2.getProductList());
		out_cluster_list.add(cluster_to_store);
		return out_cluster_list;
	}
	private boolean compare_attributes(ClusterClass cluster_1, ClusterClass cluster_2) {
		List<ProductAttributeClass> attribute_list_1 = cluster_1.getProductList().get(0).getAttributes();
		List<ProductAttributeClass> attribute_list_2 = cluster_2.getProductList().get(0).getAttributes();
		int number_match = 0;
		for(int i_iterator=0; i_iterator<attribute_list_1.size(); i_iterator++){
			boolean did_attribute_name_match = false;
			boolean did_that_value_match = false;
			for(int j_iterator=0; j_iterator<attribute_list_2.size(); j_iterator++){
				if(matchStrings(attribute_list_1.get(i_iterator).getName(), attribute_list_2.get(j_iterator).getName())){
					did_attribute_name_match = true;
					if(matchStrings(attribute_list_1.get(i_iterator).getValue(), attribute_list_2.get(j_iterator).getValue())){
						did_that_value_match = true;
					}
				}
			}
			if(did_attribute_name_match && did_that_value_match){
				number_match++;
			}
		}
		System.out.println("% match "+number_match/Math.min(attribute_list_1.size(), attribute_list_2.size()));
		if(number_match/Math.min(attribute_list_1.size(), attribute_list_2.size()) >= 0.8){
			return true;
		}else{
			return false;
		}
	}
	private boolean compare_product_names(ClusterClass cluster_1, ClusterClass cluster_2) {
		if(matchStrings(cluster_1.getProductList().get(0).getTitle(), cluster_2.getProductList().get(0).getTitle())){
			return true;
		}else{
			return false;
		}
	}
    public static boolean matchStrings(String name1, String name2){
	    if(name1.equalsIgnoreCase(name2)){
		    return true;
	    }
	    return false;
//	    StringMatching bfs = new StringMatching();
//	    boolean value;
//			
//	    String[] product_name_split = name2.split("\\s");
//            int count_title = 0;
//            float percentage=0.0f, per;
//	    float length1 = name1.split("\\s").length;
//	    float length2 = product_name_split.length;
//            float max_length = length1;
//	    
//	    if(max_length < length2){
//		    max_length = length2;
//	    }
//	    
//	    //Compare Names
//	    for (int temp = 0; temp < length2; temp++) {
//		    bfs.setString(name1, product_name_split[temp]);
//		    value = bfs.search();
//		    if (value == true) {
//			    count_title++;
//		    }
//	    }
//	    if (count_title != 0 && max_length != 0) {
//		    per = count_title / max_length;
//		    percentage = (per * 100);
//	    }
//	    
//	    if(percentage == 100.0f && length1 == length2){
//		    return true;
//	    }
//	    return false;
    }
}