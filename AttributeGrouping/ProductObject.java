package AttributeGrouping;

import java.util.ArrayList;
import java.util.List;

public class ProductObject {
	private int id;
	private String title;
	private List<ProductAttributeClass> attribute_list;
	
	ProductObject(){
		attribute_list = new ArrayList();
	}
	public void setID(int in_id){
		this.id = in_id;
	}
	public void setTitle(String in_title){
		this.title = in_title;
	}
	public void setAttributes(List<ProductAttributeClass> in_attribute_list){
		this.attribute_list = in_attribute_list;
	}
	public int getID(){
		return this.id;
	}
	public String getTitle(){
		return this.title;
	}
	public List<ProductAttributeClass> getAttributes(){
		return this.attribute_list;
	}
}