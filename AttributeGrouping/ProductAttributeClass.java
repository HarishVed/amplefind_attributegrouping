package AttributeGrouping;

public class ProductAttributeClass {
	private int id;
	private String name;
	private String value;
	
	public void setID(int in_id){
		this.id = in_id;
	}
	public void setName(String in_name){
		this.name = in_name;
	}
	public void setValue(String in_value){
		this.value = in_value;
	}
	public String getName(){
		return this.name;
	}
	public String getValue(){
		return this.value;
	}
}