package AttributeGrouping;

import java.util.ArrayList;
import java.util.List;

public class ClusterClass {
	private List<ProductObject> product_list;
	
	ClusterClass(){
		product_list = new ArrayList();
	}
	public void setProductList(List<ProductObject> in_product_list){
		this.product_list = in_product_list;
	}
	public List<ProductObject> getProductList(){
		return this.product_list;
	}
	public void addProductList(List<ProductObject> productList) {
		this.product_list.addAll(productList);
	}
}